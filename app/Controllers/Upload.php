<?php

namespace App\Controllers;

use CodeIgniter\Files\File;

class Upload extends BaseController
{
    protected $helpers = ['form'];

    public function index()
    {
        return view('Upload/upload_form', ['errors' => []]);
    }

    public function uploaded_images()
    {
        return view('Upload/uploaded_images', ['errors' => []]);
    }

    public function upload()
    {
        $img = $this->request->getFile('userfile');
        $title = $this->request->getVar('title');

        if (! $img->hasMoved()) {
            //$filepath = WRITEPATH . $img->store();
            $new_name = $img->getRandomName();
            $img->move('../public/uploads', $new_name);
            $path = '../public/uploads' . $new_name;
            $filepath = base_url() . '/public/uploads/' . $new_name;

            $data = ['uploaded_flleinfo' => new File($path)];

            $db = db_connect();
            if ($db) {
                $builder = $db->table('uploadfile');
                $arr = [
                    'imgID' => 'NULL',
                    'src' => $filepath,
                    'title' => $title
                ];
                $builder->insert($arr);
            }

            return view('Upload/upload_success', $data);
        } else {
            $data = ['errors' => 'The file has already been moved.'];

            return view('Upload/upload_form', $data);
        }
    }
}
