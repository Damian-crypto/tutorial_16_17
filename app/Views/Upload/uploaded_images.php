<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Uploaded Images</title>
    </head>
    <body>
        <table cellpadding=10>
            <th>Image</th><th>Title</th>
        <?php

            $db = db_connect();
            $builder = $db->table('uploadfile');
            $query = $builder->get();

            foreach ($query->getResult() as $row) {?>
            
            <tr>
                <td><img width=400 src="<?php echo $row->src; ?>" /></td>
                <td><?php echo $row->title; ?></td>
            </tr>

        <?php } ?>
    </body>
</html>
