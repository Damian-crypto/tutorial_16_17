<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Upload Form</title>
    </head>
    <body>

    <?php foreach ($errors as $error): ?>
        <li><?= esc($error) ?></li>
    <?php endforeach ?>

        <?= form_open_multipart('upload/upload') ?>
            <label>Upload the image file:</label>
            <input type="file" name="userfile" />
            <br /><br />
            <label>Enter a title for the image:</label>
            <input type="text" name="title" />
            <br /><br />
            <input type="submit" value="Upload" />

        </form>

        <br /><br />
        <a href="<?= base_url(). "/public/upload/uploaded_images"?>">Go to see the uploaded images</a>

    </body>
</html>
