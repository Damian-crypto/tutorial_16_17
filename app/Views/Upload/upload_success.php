<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Upload Form</title>
    </head>
    <body>

        <h1><font color="green">Your file was successfully uploaded!</font></h1>

        <p><?= anchor('upload', 'Upload Another File!') ?></p>

        <a href="<?= base_url(). "/public/upload/uploaded_images"?>">Go to see the uploaded images</a>

    </body>
</html>
